import * as functions from "firebase-functions"
import * as cors from "cors"
import * as event from "../event/event"

const corsHandler = cors({origin: true})

export const intentNewGame = "new_game"

export const newGame = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    try {
      await event.add({intent: intentNewGame})
      response.status(200).json({})
    } catch (error) {
      response.status(500).json({})
    }
  })
})
