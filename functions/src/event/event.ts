/* eslint-disable @typescript-eslint/ban-types */
import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import {Intents} from "./type"

export const collectionName = "event"

export const add = (event: object):Promise<void> => admin.firestore()
    .collection(collectionName)
    .add({...event, createdAt: admin.firestore.Timestamp.now()})
    .then(() => Promise.resolve())
    .catch((err) => Promise.reject(Error(err)))

export const observable = (intents: Intents): functions.CloudFunction<functions.firestore.QueryDocumentSnapshot> => {
  return functions.firestore
      .document(`${collectionName}/{id}`)
      .onCreate(async (snapshot) => {
        const {intent} = snapshot.data()

        await Promise.all(intents.map(async (i) => {
          if (i.intent === intent) await i.cb(snapshot.data())
        }))
      })
}
