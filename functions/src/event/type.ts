/* eslint-disable @typescript-eslint/no-explicit-any */
export type Intent = {
    intent: string,
    cb: (_: any) => Promise<void>
}

export type Intents = Intent[]
