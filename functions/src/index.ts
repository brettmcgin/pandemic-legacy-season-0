import * as admin from "firebase-admin"

admin.initializeApp()

import * as event from "./event/event"
import * as newGame from "./newgame/https"
import * as player from "./player/https"
import * as threat from "./threat/https"
import * as playerDeck from "./playerdeck/https"

import {intents as threatStateIntents} from "./threat/state"
import {intents as playerDeckStateIntents} from "./playerdeck/state"

exports.eventObserver = event.observable([
  ...threatStateIntents,
  ...playerDeckStateIntents,
])

exports.newGame = newGame.newGame

exports.playerAdd = player.add
exports.playerRename = player.rename
exports.playerRemove = player.remove

exports.playerDeckTopDraw = playerDeck.drawTopCard
exports.playerDeckDiscardCard = playerDeck.discardCard
exports.playerDeckReserveCard = playerDeck.reserveCard

exports.threatShuffle = threat.shuffle
exports.threatDrawBottomCard = threat.drawBottomCard
exports.threatDrawTopCard = threat.drawTopCard
