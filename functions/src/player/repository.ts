import * as admin from "firebase-admin"
import {Player} from "./type"

export const collectionName = "player"
const collection = admin.firestore().collection(collectionName)

export const all = (): Promise<Array<Player>> => collection.get()
    .then((snapshot) => snapshot.docs.map((d) => ({
      id: d.id,
      name: d.data().name,
    })))

export const add = (name: string): Promise<void> => collection
    .add({
      name,
      updatedAt: admin.firestore.Timestamp.now(),
    })
    .then(() => Promise.resolve())
    .catch((err) => Promise.reject(Error(err)))

export const rename = (id: string, name: string): Promise<void> => collection
    .doc(id)
    .update({
      name,
      updatedAt: admin.firestore.Timestamp.now(),
    })
    .then(() => Promise.resolve())
    .catch((err) => Promise.reject(Error(err)))

export const remove = (id: string): Promise<void> => collection
    .doc(id)
    .delete()
    .then(() => Promise.resolve())
    .catch((err) => Promise.reject(Error(err)))

export const byId = (id: string): Promise<Player> => collection.doc(id)
    .get()
    .then((p) => Promise.resolve(p.data() as Player))
    .catch((err) => Promise.reject(Error(err)))
