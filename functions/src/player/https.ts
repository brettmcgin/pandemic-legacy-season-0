import * as functions from "firebase-functions"
import * as cors from "cors"
import * as repository from "./repository"
import {isString, isEmpty} from "lodash"

const corsHandler = cors({origin: true})

export const errorJsonNoName = {error: "no name in body"}
export const errorAddPlayer = {error: "unable to save check logs"}

export const add = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const {name} = request.body
    console.log({
      name,
      body: request.body,
    })
    if (isString(name) && !isEmpty(name)) {
      try {
        await repository.add(name)
        response.status(200).json({})
      } catch (error) {
        response.status(500).json(errorAddPlayer)
      }
    } else {
      response.status(400).json(errorJsonNoName)
    }
  })
})

export const rename = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const {id, name} = request.body
    if (isString(name) && !isEmpty(name)) {
      try {
        await repository.rename(id, name)
        response.status(200).json({})
      } catch (error) {
        response.status(500).json(errorAddPlayer)
      }
    } else {
      response.status(400).json(errorJsonNoName)
    }
  })
})

export const remove = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const {id} = request.body
    try {
      await repository.remove(id)
      response.status(200).json({})
    } catch (error) {
      response.status(500).json(errorAddPlayer)
    }
  })
})
