import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import * as repository from "./repository"

export const collectionName = "playerState"

export const observer = functions.firestore
    .document(`${repository.collectionName}/{id}`)
    .onCreate(() => updateState())

const updateState = async () => {
  try {
    await admin.firestore().collection(collectionName).add({
      players: await repository.all(),
      createdAt: admin.firestore.Timestamp.now(),
    })
  } catch (error) {
    functions.logger.error(error)
  }
}
