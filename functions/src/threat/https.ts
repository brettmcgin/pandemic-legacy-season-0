import * as functions from "firebase-functions"
import * as cors from "cors"
import * as event from "../event/event"
import * as state from "./state"

const corsHandler = cors({origin: true})

export const drawTopCard = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    try {
      await event.add({intent: state.IntentDrawTopCard.intent})
      response.status(200).json({})
    } catch (error) {
      response.status(500).json({})
    }
  })
})

export const drawBottomCard = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    try {
      await event.add({intent: state.IntentDrawBottomCard.intent})
      response.status(200).json({})
    } catch (error) {
      response.status(500).json({})
    }
  })
})

export const shuffle = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    try {
      await event.add({intent: state.IntentShuffleDeck.intent})
      response.status(200).json({})
    } catch (error) {
      response.status(500).json({})
    }
  })
})
