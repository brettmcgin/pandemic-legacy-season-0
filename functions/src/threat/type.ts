import {isEmpty, last} from "lodash"

export enum Threat {
  Algiers = "algiers",
  Atlanta = "atlanta",
  Baghdad = "baghdad",
  Bangkok = "bangkok",
  Bogota = "bogota",
  Bombay = "bombay",
  BuenosAires = "buenos_aires",
  Cairo = "cairo",
  Calcutta = "calcutta",
  Delhi = "delhi",
  EastBerlin = "east_berlin",
  Hanoi = "hanoi",
  Havana = "havana",
  Istanbul = "istanbul",
  Jakarta = "jakarta",
  Johannesburg = "johannesburg",
  Karachi = "karachi",
  Khartoum = "khartoum",
  Kiev = "kiev",
  Lagos = "lagos",
  Leningrad = "leningrad",
  Leopoldville = "leopoldville",
  Lima = "lima",
  London = "london",
  LosAngeles = "los_angeles",
  Madrid = "madrid",
  Manila = "manila",
  MexicoCity = "mexico_city",
  Moscow = "moscow",
  NewYork = "new_york",
  Novosibirsk = "novosibirsk",
  Osaka = "osaka",
  Paris = "paris",
  Peking = "peking",
  Prague = "prague",
  Pyongyang = "pyongyang",
  Riyadh = "riyadh",
  Rome = "rome",
  Saigon = "saigon",
  SanFrancisco = "san_francisco",
  Santiago = "santiago",
  SaoPaulo = "sao_paulo",
  Shanghai = "shanghai",
  Sydney = "sydney",
  Tokyo = "tokyo",
  Toronto = "toronto",
  Warsaw = "warsaw",
  Washington = "washington",
}

export const threatValues = (): string[] =>
  Object.entries(Threat).map((t) => last(t)).filter((t) => !isEmpty(t)) as string[]

export type ThreatCard = {
  id: string,
  name: Threat
}

export type ThreatDeck = {
  deck: Array<ThreatCard>,
  discard: Array<ThreatCard>
}
