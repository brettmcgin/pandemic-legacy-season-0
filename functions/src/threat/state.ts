import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import * as repository from "./repository"
import {Intent, Intents} from "../event/type"
import * as deck from "./deck"
import {ThreatCard, ThreatDeck} from "./type"
import {isEmpty, isEqual, last} from "lodash"
import {intentNewGame} from "../newgame/https"

export const collectionName = "threatDeckState"

export const emptyState = (): ThreatDeck => ( {
  deck: Array<ThreatCard>(),
  discard: Array<ThreatCard>(),
})

export const currentState = async ():Promise<ThreatDeck> => admin
    .firestore()
    .collection(collectionName)
    .orderBy("createdAt", "desc")
    .limit(1)
    .get()
    .then((snapshot) => last(snapshot.docs)?.data())
    .then((data) => ({...emptyState(), ...data}))

export const updateState = async (threatDeck: ThreatDeck): Promise<void> => {
  try {
    const current = await currentState()
    if (isEqual(threatDeck.deck, current.deck) && isEqual(threatDeck.discard, current.deck)) return

    const timeStampedState = {...threatDeck, ...{createdAt: admin.firestore.Timestamp.now()}}

    await admin.firestore().collection(collectionName).add(timeStampedState)
  } catch (error) {
    functions.logger.error(error)
  }
}

const shuffleDeck = async ():Promise<void> => updateState(deck.shuffleDeck(await currentState()))
const drawTopCard = async ():Promise<void> => updateState(deck.drawTopCard(await currentState()))
const drawBottomCard = async ():Promise<void> => updateState(deck.drawBottomCard(await currentState()))

const initialState = async () =>({
  ...emptyState(),
  ...{
    deck: await repository.all(),
    discard: emptyState().discard,
  },
})

export const newGame = async (): Promise<void> => {
  const state = await currentState()

  if (isEmpty(state.deck) && isEmpty(state.discard)) {
    await updateState(await initialState())
  } else await shuffleDeck()
}

const IntentNewGame: Intent = {intent: intentNewGame, cb: newGame}
export const IntentDrawTopCard: Intent = {intent: "threat_draw_top_card", cb: drawTopCard}
export const IntentDrawBottomCard: Intent = {intent: "threat_draw_bottom_card", cb: drawBottomCard}
export const IntentShuffleDeck: Intent = {intent: "threat_shuffle", cb: shuffleDeck}

export const intents: Intents = [
  IntentNewGame,
  IntentDrawTopCard,
  IntentDrawBottomCard,
  IntentShuffleDeck,
]
