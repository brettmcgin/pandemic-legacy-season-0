import * as admin from "firebase-admin"
import {once, isEmpty} from "lodash"
import {ThreatCard, threatValues} from "./type"

export const collectionName = "threatDeck"
const collection = admin.firestore().collection(collectionName)

const add = (name: string) => collection.add({
  name,
  createdAt: admin.firestore.Timestamp.now(),
})

const init = (): Promise<void> => collection.get()
    .then(async (snapshot) => {
      if (isEmpty(snapshot?.docs)) {
        await Promise.all(threatValues().map((t) => add(t)))
      }
    })
    .catch((error) => Promise.reject(Error(error)))

const initOnce = once(init)

export const all = async (): Promise<Array<ThreatCard>> => {
  await initOnce()

  return collection.get()
      .then((snapshot) => snapshot.docs.map((d) => ({
        id: d.id,
        name: d.data().name,
      })))
}
