import {shuffle, last, drop, dropRight, first} from "lodash"
import {ThreatCard, ThreatDeck} from "./type"

const emtpy = Array<ThreatCard>()

const appendToEnd = (deck: ThreatCard[], card: ThreatCard | undefined) :ThreatCard[] =>
card !== undefined ? [...deck, card] :[...deck]

export const shuffleDeck = (current: ThreatDeck): ThreatDeck => ({
  deck: [...current.deck, ...shuffle(current.discard)],
  discard: emtpy,
} as ThreatDeck)

export const drawTopCard = (current: ThreatDeck): ThreatDeck => ({
  deck: dropRight(current.deck),
  discard: appendToEnd(current.discard, last(current.deck)),
} as ThreatDeck)

export const drawBottomCard = (current: ThreatDeck): ThreatDeck => ({
  deck: drop(current.deck),
  discard: appendToEnd(current.discard, first(current.deck)),
} as ThreatDeck)
