/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import * as functions from "firebase-functions"
import * as admin from "firebase-admin"
import * as repository from "./repository"
import * as playerRepository from "../player/repository"
import {Intent, Intents} from "../event/type"
import * as deck from "./deck"
import {PlayerCard,
  PlayerDeck,
  isEscalationCard,
  City,
  europeCities,
  africaCities,
  northAmericaCities,
  asiaCities,
  southAmericaCities,
  pacificRimCities} from "./type"
import {isEqual, isEqualWith, last, includes} from "lodash"
import {intentNewGame} from "../newgame/https"

export const collectionName = "playerDeckState"

type PlayerHand = {
  player: {
    id: string,
    name: string,
  }
  hand: PlayerDeck
}

type PlayerCardsState = {
  deck: PlayerDeck,
  discard: PlayerDeck,
  hands: PlayerHand[],
  reserve: PlayerDeck,
}

export const emptyState = (): PlayerCardsState => ( {
  deck: Array<PlayerCard>(),
  discard: Array<PlayerCard>(),
  hands: Array<PlayerHand>(),
  reserve: Array<PlayerCard>(),
})

const emptyPlayerHands = async () => (await playerRepository.all())
    .map((p) => ({
      hand: Array<PlayerCard>(),
      player: {
        id: p.id,
        name: p.name,
      },
    }) as PlayerHand)

const initialState = async (): Promise<PlayerCardsState> =>({
  ...emptyState(),
  ...{
    deck: deck.shuffle(await repository.all()),
    hands: await emptyPlayerHands(),
  },
})

export const currentState = async ():Promise<PlayerCardsState> => admin
    .firestore()
    .collection(collectionName)
    .orderBy("createdAt", "desc")
    .limit(1)
    .get()
    .then((snapshot) => last(snapshot.docs)?.data())
    .then((data) => ({...emptyState(), ...data}))

const playerCardsStateCustomizer = (first:PlayerCardsState, second:PlayerCardsState) =>
  isEqual(first.deck, second.deck) &&
  isEqual(first.discard, second.discard) &&
  isEqual(first.hands, second.hands)

export const updateState = async (playerCardsState: PlayerCardsState): Promise<void> => {
  try {
    if (isEqualWith(playerCardsState, await currentState(), playerCardsStateCustomizer)) return

    const timeStampedState = {...playerCardsState, ...{createdAt: admin.firestore.Timestamp.now()}}

    await admin.firestore().collection(collectionName).add(timeStampedState)
  } catch (error) {
    functions.logger.error(error)
  }
}

export const newGame = async ():Promise<void> => updateState(await initialState())


export const drawTopCard = async (event: any):Promise<void> => {
  const {playerId} = event

  const current = await currentState()
  const modifiedDeck = deck.drawTopCard(current.deck)

  if (isEscalationCard(modifiedDeck.playerCard)) {
    return updateState({
      deck: modifiedDeck.playerDeck,
      discard: deck.appendToEnd(current.discard, modifiedDeck.playerCard),
      hands: current.hands,
      reserve: current.reserve,
    })
  }

  const updatedHands = current.hands.map((playerHand) =>
    playerHand.player.id === playerId? {
      ...playerHand,
      ...{hand: deck.appendToEnd(playerHand.hand, modifiedDeck.playerCard)},
    }: playerHand
  )

  await updateState({
    deck: modifiedDeck.playerDeck,
    discard: current.discard,
    hands: updatedHands,
    reserve: current.reserve,
  })
}

const findPlayerCard = (cardId: string, hands: PlayerHand[]):{
  hands: PlayerHand[], card: PlayerCard | undefined
} => {
  let card: PlayerCard | undefined = undefined

  const filtered: PlayerHand[] = hands.map((ph) => ({
    ...ph,
    hand: ph.hand.filter((c) => {
      if (c.id === cardId) card = c
      return c.id !== cardId
    }),
  }))

  return {
    hands: filtered,
    card,
  }
}

export const discardCard = async (event: any) => {
  const {cardId} = event
  const current = await currentState()
  const updated = findPlayerCard(cardId, current.hands)

  await updateState({
    deck: current.deck,
    discard: deck.appendToEnd(current.discard, updated.card),
    hands: updated.hands,
    reserve: current.reserve,
  })
}

const citiesByRegion = (region: string): City[] => {
  if (region === "europe") return europeCities
  if (region === "africa") return africaCities
  if (region === "north_america") return northAmericaCities
  if (region === "asia") return asiaCities
  if (region === "south_america") return southAmericaCities
  if (region === "pacific_rim") return pacificRimCities

  return Array<City>()
}

const findReserveCard = (region: string, deck: PlayerCard[]): {
  card: PlayerCard | undefined, deck: PlayerDeck
} => {
  const cities = citiesByRegion(region)
  const card = deck.find((c) => includes(cities, c.name))

  console.log({
    card,
    deck: deck.filter((c) => c.id !== card?.id),

  })

  return {
    card,
    deck: deck.filter((c) => c.id !== card?.id),
  }
}

export const reserveCard = async (event: any) => {
  const {region} = event
  const current = await currentState()

  const modified = findReserveCard(region, current.deck)
  const reserve = deck.appendToEnd(current.reserve, modified.card)

  await updateState({
    deck: deck.shuffle(modified.deck),
    discard: current.discard,
    hands: current.hands,
    reserve,
  })
}

const IntentNewGame: Intent = {intent: intentNewGame, cb: newGame}
export const IntentDrawTopCard: Intent = {intent: "player_deck_draw_top_card", cb: drawTopCard}
export const IntentDiscardCard: Intent = {intent: "player_deck_discard_card", cb: discardCard}
export const IntentReserveCard: Intent = {intent: "player_deck_reserve_card", cb: reserveCard}

export const intents: Intents = [
  IntentNewGame,
  IntentDrawTopCard,
  IntentDiscardCard,
  IntentReserveCard,
]
