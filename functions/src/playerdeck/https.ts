import * as functions from "firebase-functions"
import * as cors from "cors"
import * as event from "../event/event"
import * as state from "./state"
import {byId} from "../player/repository"
import {regions} from "./type"
import {isEmpty, includes} from "lodash"

const corsHandler = cors({origin: true})

const errorNoPlayerFound = {error: "no player found"}
const errorNoCardId = {error: "no cardId"}
const errorIncorrectRegion = {error: `region must be one of the following: ${regions}`}

export const drawTopCard = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    try {
      const {playerId} = request.body
      console.log({playerId})
      const player = await byId(playerId)
      if (isEmpty(player)) {
        response.status(400).json(errorNoPlayerFound)
      } else {
        await event.add({intent: state.IntentDrawTopCard.intent, playerId})
        response.status(200).json({})
      }
    } catch (error) {
      functions.logger.error(error)
      response.status(500).json(errorNoPlayerFound)
    }
  })
})

export const discardCard = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const {cardId} = request.body
    if (isEmpty(cardId)) {
      response.status(400).json(errorNoCardId)
    } else {
      await event.add({intent: state.IntentDiscardCard.intent, cardId})
      response.status(200).json({})
    }
  })
})

export const reserveCard = functions.https.onRequest((request, response) => {
  corsHandler(request, response, async () => {
    const {region} = request.body

    if (!includes(regions, region)) {
      response.status(400).json(errorIncorrectRegion)
    } else {
      await event.add({intent: state.IntentReserveCard.intent, region})
      response.status(200).json({})
    }
  })
})
