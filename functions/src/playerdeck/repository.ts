import * as admin from "firebase-admin"
import {once, times, isEmpty} from "lodash"
import {PlayerCard, cityValues, eventValues, EscalationCard} from "./type"

export const collectionName = "playerDeck"
const collection = admin.firestore().collection(collectionName)

const add = (name: string) => collection.add({
  name,
  createdAt: admin.firestore.Timestamp.now(),
})

const init = (): Promise<void> => collection.get()
    .then(async (snapshot) => {
      const tempEscalationCard : EscalationCard = {
        id: "id",
        name: "escalation",
      }

      if (isEmpty(snapshot?.docs)) {
        await Promise.all(
            [
              ...cityValues(),
              ...eventValues(),
              ...times(5, () => tempEscalationCard.name),
            ].map((t) => add(t))
        )
      }
    })
    .catch((error) => Promise.reject(Error(error)))

const initOnce = once(init)

export const all = async (): Promise<Array<PlayerCard>> => {
  await initOnce()

  return collection.get()
      .then((snapshot) => snapshot.docs.map((d) => ({
        id: d.id,
        name: d.data().name,
      })))
}
