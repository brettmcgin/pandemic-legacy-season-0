import {isEmpty, last} from "lodash"

export type EscalationCard = {
  id: string,
  name: "escalation"
}

export const isEscalationCard = (card: PlayerCard | undefined): boolean =>
  card !== undefined && card.name === "escalation"

export enum Event {
  AHoldoverFromTheWar = "event_a_holdover_from_the_war",
  Airlift = "event_airlift",
  CounterIntelligence = "event_counter_intelligence",
  CoverOfDarkness = "event_cover_of_darkness",
  Forecast = "event_forecast",
}

export const eventValues = (): string[] =>
  Object.entries(Event).map((t) => last(t)).filter((t) => !isEmpty(t)) as string[]

export type EventCard = {
  id: string,
  name: Event
}

export enum City {
  Algiers = "city_algiers",
  Atlanta = "city_atlanta",
  Baghdad = "city_baghdad",
  Bangkok = "city_bangkok",
  Bogota = "city_bogota",
  Bombay = "city_bombay",
  BuenosAires = "city_buenos_aires",
  Cairo = "city_cairo",
  Calcutta = "city_calcutta",
  Delhi = "city_delhi",
  EastBerlin = "city_east_berlin",
  Hanoi = "city_hanoi",
  Havana = "city_havana",
  Istanbul = "city_istanbul",
  Jakarta = "city_jakarta",
  Johannesburg = "city_johannesburg",
  Karachi = "city_karachi",
  Khatoum = "city_khatoum",
  Kiev = "city_kiev",
  Lagos = "city_lagos",
  Leningrad = "city_leningrad",
  Leopoldville = "city_leopoldville",
  Lima = "city_lima",
  London = "city_london",
  LosAngeles = "city_los_angeles",
  Madrid = "city_madrid",
  Manila = "city_manila",
  MexicoCity = "city_mexico_city",
  Moscow = "city_moscow",
  NewYork = "city_new_york",
  Novosibirsk = "city_novosibirsk",
  Osaka = "city_osaka",
  Paris = "city_paris",
  Peking = "city_peking",
  Prague = "city_prague",
  Pyongyang = "city_pyongyang",
  Riyadh = "city_riyadh",
  Rome = "city_rome",
  Saigon = "city_saigon",
  SanFancisco = "city_san_fancisco",
  Santiago = "city_santiago",
  SaoPaulo = "city_sao_paulo",
  Shanghai = "city_shanghai",
  Sydney = "city_sydney",
  Tokyo = "city_tokyo",
  Toronto = "city_toronto",
  Warsaw = "city_warsaw",
  Washington = "city_washington",
}

export type CityCard = {
  id: string,
  name: City,
}

export const regions = ["europe", "africa", "north_america", "asia", "south_america", "pacific_rim"]
export const europeCities = [
  City.Istanbul,
  City.Paris,
  City.Prague,
  City.Rome,
  City.Leningrad,
  City.Madrid,
  City.London,
  City.Moscow,
  City.Warsaw,
  City.EastBerlin,
  City.Kiev,
]

export const africaCities = [
  City.Leopoldville,
  City.Lagos,
  City.Cairo,
  City.Algiers,
  City.Johannesburg,
  City.Khatoum,
]

export const northAmericaCities = [
  City.SanFancisco,
  City.LosAngeles,
  City.MexicoCity,
  City.Washington,
  City.Toronto,
  City.Atlanta,
  City.NewYork,
  City.Havana,
]

export const asiaCities = [
  City.Novosibirsk,
  City.Saigon,
  City.Riyadh,
  City.Pyongyang,
  City.Peking,
  City.Bombay,
  City.Baghdad,
  City.Delhi,
  City.Hanoi,
  City.Shanghai,
  City.Bangkok,
  City.Calcutta,
  City.Karachi,
]

export const southAmericaCities = [
  City.BuenosAires,
  City.Lima,
  City.Bogota,
  City.Santiago,
  City.SaoPaulo,
]

export const pacificRimCities = [
  City.Osaka,
  City.Manila,
  City.Tokyo,
  City.Sydney,
  City.Jakarta,
]

export const cityValues = (): string[] =>
  Object.entries(City).map((t) => last(t)).filter((t) => !isEmpty(t)) as string[]

export type PlayerCard = EventCard | CityCard | EscalationCard

export type PlayerDeck = PlayerCard[]
