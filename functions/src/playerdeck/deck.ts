import {shuffle as lodashShuffle, last, dropRight, flatten, times} from "lodash"
import {PlayerCard, PlayerDeck, isEscalationCard} from "./type"

const empty = () => Array<PlayerCard>()

export const appendToEnd = (deck: PlayerDeck, card: PlayerCard | undefined) :PlayerDeck =>
card !== undefined ? [...deck, card] :[...deck]

export const shuffle =(deck: PlayerDeck): PlayerDeck => {
  const escalationCards = deck.filter((c) => isEscalationCard(c))
  const playerCards = lodashShuffle(deck.filter((c) => !isEscalationCard(c)))

  const miniDecks = times(escalationCards.length, () => empty())
  if (miniDecks.length === 0) return deck
  playerCards.forEach((p, i) => miniDecks[i%escalationCards.length].push(p))

  return flatten(miniDecks.map((m, i) => lodashShuffle([...m, escalationCards[i]]))
      .reverse())
}

export const drawTopCard = (playerDeck: PlayerDeck): {
  playerDeck: PlayerDeck,
  playerCard: PlayerCard | undefined
} => ({playerDeck: dropRight(playerDeck), playerCard: last(playerDeck)})
