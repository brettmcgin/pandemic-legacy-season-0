import test from "ava"
import {includes} from "lodash"
import {shuffleDeck, drawTopCard, drawBottomCard} from "../src/threat/deck"
import {ThreatDeck, Threat, threatValues} from "../src/threat/type"

test("test enum value", (t) => {
  t.true(includes(threatValues(), Threat.Algiers))
})

test("shuffle only discarded cards", (t) => {
  const before = {
    deck: [],
    discard: [{id: "2", name: Threat.Atlanta}],
  } as ThreatDeck

  const after = shuffleDeck(before)
  t.true(after.discard.length === 0)
  t.true(after.deck.length === 1)
})

test("shuffle preserves deck order", (t) => {
  const before = {
    deck: [
      {id: "1", name: Threat.Algiers},
      {id: "2", name: Threat.Atlanta},
      {id: "3", name: Threat.Baghdad},
      {id: "4", name: Threat.Bangkok},
    ],
    discard: [{id: "5", name: Threat.Bogota}],
  } as ThreatDeck

  const after = shuffleDeck(before)
  t.is(0, after.discard.length)
  t.is(5, after.deck.length)
  t.is("1", after.deck[0].id)
  t.is("2", after.deck[1].id)
  t.is("3", after.deck[2].id)
  t.is("4", after.deck[3].id)
  t.is("5", after.deck[4].id)
})

test("drawTopCard takes end deck card and places it on end of discard", (t) => {
  const before = {
    deck: [
      {id: "1", name: Threat.Algiers},
      {id: "2", name: Threat.Atlanta},
    ],
    discard: [],
  } as ThreatDeck

  const after = drawTopCard(before)
  t.is(1, after.deck.length)
  t.is(1, after.discard.length)
  t.is("2", after.discard[0].id)
})

test("drawBottomCard takes start deck card and places it on end of discard", (t) => {
  const before = {
    deck: [
      {id: "1", name: Threat.Algiers},
      {id: "2", name: Threat.Atlanta},
    ],
    discard: [],
  } as ThreatDeck

  const after = drawBottomCard(before)
  t.is(1, after.deck.length)
  t.is("2", after.deck[0].id)
  t.is(1, after.discard.length)
  t.is("1", after.discard[0].id)
})
