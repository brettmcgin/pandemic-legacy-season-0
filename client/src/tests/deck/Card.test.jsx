import React from 'react'
import { render, screen } from '@testing-library/react'
import Card from '../../deck/Card'

test('show a player card', () => {
  render(<Card name="player_back" />)
  expect(screen.getByAltText(/player_back/i)).toBeInTheDocument()
})

test('show a threat card', () => {
  render(<Card name="bangkok" />)
  expect(screen.getByAltText(/bangkok/i)).toBeInTheDocument()
})
