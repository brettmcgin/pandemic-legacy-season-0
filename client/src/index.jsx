import React from 'react'
import ReactDOM from 'react-dom'
import './main.css'
import Add from './playerdeck/Add'
import Players from './playerdeck/Players'
import Threat from './threatdeck/Deck'
import NewGame from './newgame/NewGame'
import PlayerDeck from './playerdeck/Deck'

ReactDOM.render(
  <React.StrictMode>
    <div className="bg-gray-500 min-w-full min-h-screen flex">
      <div className="auto-cols-max flex-grow">
        <Players />
      </div>

      <div className="flex-none">
        <NewGame />
        <Add />
        <Threat />
        <PlayerDeck />
      </div>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
