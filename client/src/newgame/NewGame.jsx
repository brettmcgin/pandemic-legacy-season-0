import React from 'react'
import { isDevelopment } from '../firebase/firebase'
import { post } from '../util/https'

const url = isDevelopment
  ? 'http://localhost:5001/pandemic-legacy-season-0/us-central1/newGame'
  : 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/newGame'

export default function NewGame() {
  return (
    <button
      className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
      type="button"
      onClick={async () => post(url)}
    >
      New Game
    </button>
  )
}
