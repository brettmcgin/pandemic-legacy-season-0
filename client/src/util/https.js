/* eslint-disable import/prefer-default-export */
/* eslint-disable no-console */
import { isEmpty } from 'lodash'

const emptyBody = {
  method: 'post',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({}),
}

export const post = (url, body) =>
  fetch(url, {
    ...emptyBody,
    ...{ body: JSON.stringify(isEmpty(body) ? {} : body) },
  })
    .then((response) => response.json())
    .then((response) => console.log({ url, response }))
    .catch((error) => console.error({ error }))
