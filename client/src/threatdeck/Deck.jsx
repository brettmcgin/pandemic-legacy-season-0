/* eslint-disable no-console */
import React, { useState, useEffect } from 'react'
import { isEmpty, last } from 'lodash'
import firestore, { isDevelopment } from '../firebase/firebase'
import Card from '../deck/Card'
import { post } from '../util/https'

const devUrls = {
  shuffle: 'http://localhost:5001/pandemic-legacy-season-0/us-central1/threatShuffle',
  drawBottom: 'http://localhost:5001/pandemic-legacy-season-0/us-central1/threatDrawBottomCard',
  drawTop: 'http://localhost:5001/pandemic-legacy-season-0/us-central1/threatDrawTopCard',
}

const prodUrls = {
  shuffle: 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/threatShuffle',
  drawBottom: 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/threatDrawBottomCard',
  drawTop: 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/threatDrawTopCard',
}

const urls = isDevelopment ? devUrls : prodUrls

export default function Threat() {
  const [state, setState] = useState({
    deck: [],
    discard: [],
  })

  useEffect(() => {
    firestore
      .collection('threatDeckState')
      .orderBy('createdAt', 'desc')
      .limit(1)
      .onSnapshot((snapshot) => {
        const lastDoc = last(snapshot.docs)?.data()
        const newDeck = lastDoc?.deck
        const newDiscard = lastDoc?.discard

        setState({ deck: newDeck, discard: newDiscard })
      })
  }, [])

  console.log({ threatDeck: state })

  const drawDeck = () => <Card isPlayerCard={false} name="threat_back" shouldFade={isEmpty(state.deck)} />

  const discardDeck = () => (
    <Card name={last(state.discard)?.name || ''} isPlayerCard={false} shouldFade={isEmpty(state.discard)} />
  )

  const shuffle = () => post(urls.shuffle)
  const drawTopCard = () => post(urls.drawTop)
  const drawBottomCard = () => post(urls.drawBottom)

  return (
    <div>
      <div>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          type="button"
          onClick={async () => shuffle()}
        >
          Shuffle
        </button>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          type="button"
          onClick={async () => drawTopCard()}
        >
          Draw Top Card
        </button>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
          type="button"
          onClick={async () => drawBottomCard()}
        >
          Draw Bottom Card
        </button>
      </div>
      <div className="grid grid-cols-2">
        {drawDeck()}
        {discardDeck()}
      </div>
    </div>
  )
}
