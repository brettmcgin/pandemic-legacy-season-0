import firebase from 'firebase/app'
import 'firebase/firestore'
import config from './firebaseConfig'

export const isDevelopment = process.env.NODE_ENV === 'development'

firebase.initializeApp(config)

const firestore = firebase.firestore()
if (isDevelopment) firestore.useEmulator('localhost', 8080)

export default firestore
