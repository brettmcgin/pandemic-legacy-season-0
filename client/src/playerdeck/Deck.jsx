/* eslint-disable no-console */
import React, { useState, useEffect } from 'react'
import { isEmpty, last } from 'lodash'
import firestore from '../firebase/firebase'
import Card from '../deck/Card'

export default function PlayerDeck() {
  const [state, setState] = useState({
    deck: [],
    discard: [],
  })

  useEffect(() => {
    firestore
      .collection('playerDeckState')
      .orderBy('createdAt', 'desc')
      .limit(1)
      .onSnapshot((snapshot) => {
        const lastDoc = last(snapshot.docs)?.data()
        const { deck, discard } = lastDoc
        setState({ deck, discard })
      })
  }, [])

  console.log({ playerDeck: state })

  const drawDeck = () => <Card name="player_back" shouldFade={isEmpty(state.deck)} />

  const discardDeck = () => <Card name={last(state.discard)?.name || ''} shouldFade={isEmpty(state.discard)} />

  return (
    <div className="grid grid-cols-2">
      {drawDeck()}
      {discardDeck()}
    </div>
  )
}
