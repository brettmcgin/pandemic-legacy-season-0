/* eslint-disable no-console */
import React, { useState, useEffect, Fragment } from 'react'
import { isEmpty, isEqual, last } from 'lodash'
import firestore, { isDevelopment } from '../firebase/firebase'
import Card from '../deck/Card'
import { post } from '../util/https'

const devUrls = {
  drawTopCard: 'http://localhost:5001/pandemic-legacy-season-0/us-central1/playerDeckTopDraw',
  discardCard: 'http://localhost:5001/pandemic-legacy-season-0/us-central1/playerDeckDiscardCard',
}

const prodUrls = {
  drawTopCard: 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/playerDeckTopDraw',
  discardCard: 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/playerDeckDiscardCard',
}

const urls = isDevelopment ? devUrls : prodUrls

export default function Players() {
  const [state, setState] = useState([])

  useEffect(() => {
    firestore
      .collection('playerDeckState')
      .orderBy('createdAt', 'desc')
      .limit(1)
      .onSnapshot((snapshot) => {
        if (snapshot.docs.length > 0) {
          const p = last(snapshot.docs).data()
          if (!isEqual(Players, p)) setState(p)
        }
      })
  }, [])

  const drawTopCard = (playerId) => post(urls.drawTopCard, { playerId })
  const discardCard = (cardId) => post(urls.discardCard, { cardId })

  const handDiv = ({ player, hand }) => (
    <div key={player.id}>
      <div className="text-3xl text-center">{player.name}</div>
      <button
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        type="button"
        onClick={async () => drawTopCard(player?.id)}
      >
        DrawCard
      </button>
      <ul>
        {hand?.map((h) => (
          <li key={h.id}>
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              type="button"
              onClick={async () => discardCard(h.id)}
            >
              Discard
            </button>
            <Card name={h.name} />
          </li>
        ))}
      </ul>
    </div>
  )

  const handsList = (hands) => <div className="flex flex-row">{hands.map((h) => handDiv(h))}</div>

  return isEmpty(state) ? <></> : handsList(state.hands)
}
