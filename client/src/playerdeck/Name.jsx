import { string } from 'prop-types'
import React from 'react'

export default function Player({ name }) {
  return (
    <div>
      <h1>{name}</h1>
    </div>
  )
}

Player.propTypes = { name: string.isRequired }
