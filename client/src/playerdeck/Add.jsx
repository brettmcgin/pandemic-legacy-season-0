/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react'
import { isDevelopment } from '../firebase/firebase'
import { post } from '../util/https'

const url = isDevelopment
  ? 'http://localhost:5001/pandemic-legacy-season-0/us-central1/playerAdd'
  : 'https://us-central1-pandemic-legacy-season-0.cloudfunctions.net/playerAdd'

export default function Add() {
  const [name, setName] = useState('')

  const addPlayer = (event) => {
    event.preventDefault()

    post(url, { name })
  }

  return (
    <form onSubmit={(e) => addPlayer(e)}>
      <input
        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        type="submit"
        value="Add Player"
      />
      <label>
        <input id="" type="text" onChange={(e) => setName(e.target.value)} />
      </label>
    </form>
  )
}
