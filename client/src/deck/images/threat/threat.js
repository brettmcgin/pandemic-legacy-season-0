import algiers from './algiers.jpg'
import atlanta from './atlanta.jpg'
import threatBack from './back.jpg'
import baghdad from './baghdad.jpg'
import bangkok from './bangkok.jpg'
import bogota from './bogota.jpg'
import bombay from './bombay.jpg'
import buenosAires from './buenos_aires.jpg'
import cairo from './cairo.jpg'
import calcutta from './calcutta.jpg'
import delhi from './delhi.jpg'
import eastBerlin from './east_berlin.jpg'
import hanoi from './hanoi.jpg'
import havana from './havana.jpg'
import istanbul from './istanbul.jpg'
import jakarta from './jakarta.jpg'
import johannesburg from './johannesburg.jpg'
import karachi from './karachi.jpg'
import khartoum from './khartoum.jpg'
import kiev from './kiev.jpg'
import lagos from './lagos.jpg'
import leningrad from './leningrad.jpg'
import leopoldville from './leopoldville.jpg'
import lima from './lima.jpg'
import london from './london.jpg'
import losAngeles from './los_angeles.jpg'
import madrid from './madrid.jpg'
import manila from './manila.jpg'
import mexicoCity from './mexico_city.jpg'
import moscow from './moscow.jpg'
import newYork from './new_york.jpg'
import novosibirsk from './novosibirsk.jpg'
import osaka from './osaka.jpg'
import paris from './paris.jpg'
import peking from './peking.jpg'
import prague from './prague.jpg'
import pyongyang from './pyongyang.jpg'
import riyadh from './riyadh.jpg'
import rome from './rome.jpg'
import saigon from './saigon.jpg'
import sanFrancisco from './san_francisco.jpg'
import santiago from './santiago.jpg'
import saoPaulo from './sao_paulo.jpg'
import shanghai from './shanghai.jpg'
import sydney from './sydney.jpg'
import tokyo from './tokyo.jpg'
import toronto from './toronto.jpg'
import warsaw from './warsaw.jpg'
import washington from './washington.jpg'

const imageMap = [
  { name: 'threat_back', image: threatBack },
  { name: 'algiers', image: algiers },
  { name: 'atlanta', image: atlanta },
  { name: 'baghdad', image: baghdad },
  { name: 'bangkok', image: bangkok },
  { name: 'bogota', image: bogota },
  { name: 'bombay', image: bombay },
  { name: 'buenos_aires', image: buenosAires },
  { name: 'cairo', image: cairo },
  { name: 'calcutta', image: calcutta },
  { name: 'delhi', image: delhi },
  { name: 'east_berlin', image: eastBerlin },
  { name: 'hanoi', image: hanoi },
  { name: 'havana', image: havana },
  { name: 'istanbul', image: istanbul },
  { name: 'jakarta', image: jakarta },
  { name: 'johannesburg', image: johannesburg },
  { name: 'karachi', image: karachi },
  { name: 'khartoum', image: khartoum },
  { name: 'kiev', image: kiev },
  { name: 'lagos', image: lagos },
  { name: 'leningrad', image: leningrad },
  { name: 'leopoldville', image: leopoldville },
  { name: 'lima', image: lima },
  { name: 'london', image: london },
  { name: 'los_angeles', image: losAngeles },
  { name: 'madrid', image: madrid },
  { name: 'manila', image: manila },
  { name: 'mexico_city', image: mexicoCity },
  { name: 'moscow', image: moscow },
  { name: 'new_york', image: newYork },
  { name: 'novosibirsk', image: novosibirsk },
  { name: 'osaka', image: osaka },
  { name: 'paris', image: paris },
  { name: 'peking', image: peking },
  { name: 'prague', image: prague },
  { name: 'pyongyang', image: pyongyang },
  { name: 'riyadh', image: riyadh },
  { name: 'rome', image: rome },
  { name: 'saigon', image: saigon },
  { name: 'san_francisco', image: sanFrancisco },
  { name: 'santiago', image: santiago },
  { name: 'sao_paulo', image: saoPaulo },
  { name: 'shanghai', image: shanghai },
  { name: 'sydney', image: sydney },
  { name: 'tokyo', image: tokyo },
  { name: 'toronto', image: toronto },
  { name: 'warsaw', image: warsaw },
  { name: 'washington', image: washington },
]

export const back = imageMap[0]

export const names = imageMap.map((i) => i.name)

export default imageMap
