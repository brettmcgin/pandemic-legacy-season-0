import playerBack from './back.jpg'
import cityAlgiers from './city_algiers.jpg'
import cityAtlanta from './city_atlanta.jpg'
import cityBaghdad from './city_baghdad.jpg'
import cityBangkok from './city_bangkok.jpg'
import cityBogota from './city_bogota.jpg'
import cityBombay from './city_bombay.jpg'
import cityBuenosAires from './city_buenos_aires.jpg'
import cityCairo from './city_cairo.jpg'
import cityCalcutta from './city_calcutta.jpg'
import cityDelhi from './city_delhi.jpg'
import cityEastBerlin from './city_east_berlin.jpg'
import cityHanoi from './city_hanoi.jpg'
import cityHavana from './city_havana.jpg'
import cityIstanbul from './city_istanbul.jpg'
import cityJakarta from './city_jakarta.jpg'
import cityJohannesburg from './city_johannesburg.jpg'
import cityKarachi from './city_karachi.jpg'
import cityKhatoum from './city_khatoum.jpg'
import cityKiev from './city_kiev.jpg'
import cityLagos from './city_lagos.jpg'
import cityLeningrad from './city_leningrad.jpg'
import cityLeopoldville from './city_leopoldville.jpg'
import cityLima from './city_lima.jpg'
import cityLondon from './city_london.jpg'
import cityLosAngeles from './city_los_angeles.jpg'
import cityMadrid from './city_madrid.jpg'
import cityManila from './city_manila.jpg'
import cityMexicoCity from './city_mexico_city.jpg'
import cityMoscow from './city_moscow.jpg'
import cityNewYork from './city_new_york.jpg'
import cityNovosibirsk from './city_novosibirsk.jpg'
import cityOsaka from './city_osaka.jpg'
import cityParis from './city_paris.jpg'
import cityPeking from './city_peking.jpg'
import cityPrague from './city_prague.jpg'
import cityPyongyang from './city_pyongyang.jpg'
import cityRiyadh from './city_riyadh.jpg'
import cityRome from './city_rome.jpg'
import citySaigon from './city_saigon.jpg'
import citySanFancisco from './city_san_fancisco.jpg'
import citySantiago from './city_santiago.jpg'
import citySaoPaulo from './city_sao_paulo.jpg'
import cityShanghai from './city_shanghai.jpg'
import citySydney from './city_sydney.jpg'
import cityTokyo from './city_tokyo.jpg'
import cityToronto from './city_toronto.jpg'
import cityWarsaw from './city_warsaw.jpg'
import cityWashington from './city_washington.jpg'
import escalation from './escalation.jpg'
import eventAHoldoverFromTheWar from './event_a_holdover_from_the_war.jpg'
import eventAirlift from './event_airlift.jpg'
import eventCounterIntelligence from './event_counter_intelligence.jpg'
import eventCoverOfDarkness from './event_cover_of_darkness.jpg'
import eventForecast from './event_forecast.jpg'

const imageMap = [
  { name: 'player_back', image: playerBack },
  { name: 'city_algiers', image: cityAlgiers },
  { name: 'city_atlanta', image: cityAtlanta },
  { name: 'city_baghdad', image: cityBaghdad },
  { name: 'city_bangkok', image: cityBangkok },
  { name: 'city_bogota', image: cityBogota },
  { name: 'city_bombay', image: cityBombay },
  { name: 'city_buenos_aires', image: cityBuenosAires },
  { name: 'city_cairo', image: cityCairo },
  { name: 'city_calcutta', image: cityCalcutta },
  { name: 'city_delhi', image: cityDelhi },
  { name: 'city_east_berlin', image: cityEastBerlin },
  { name: 'city_hanoi', image: cityHanoi },
  { name: 'city_havana', image: cityHavana },
  { name: 'city_istanbul', image: cityIstanbul },
  { name: 'city_jakarta', image: cityJakarta },
  { name: 'city_johannesburg', image: cityJohannesburg },
  { name: 'city_karachi', image: cityKarachi },
  { name: 'city_khatoum', image: cityKhatoum },
  { name: 'city_kiev', image: cityKiev },
  { name: 'city_lagos', image: cityLagos },
  { name: 'city_leningrad', image: cityLeningrad },
  { name: 'city_leopoldville', image: cityLeopoldville },
  { name: 'city_lima', image: cityLima },
  { name: 'city_london', image: cityLondon },
  { name: 'city_los_angeles', image: cityLosAngeles },
  { name: 'city_madrid', image: cityMadrid },
  { name: 'city_manila', image: cityManila },
  { name: 'city_mexico_city', image: cityMexicoCity },
  { name: 'city_moscow', image: cityMoscow },
  { name: 'city_new_york', image: cityNewYork },
  { name: 'city_novosibirsk', image: cityNovosibirsk },
  { name: 'city_osaka', image: cityOsaka },
  { name: 'city_paris', image: cityParis },
  { name: 'city_peking', image: cityPeking },
  { name: 'city_prague', image: cityPrague },
  { name: 'city_pyongyang', image: cityPyongyang },
  { name: 'city_riyadh', image: cityRiyadh },
  { name: 'city_rome', image: cityRome },
  { name: 'city_saigon', image: citySaigon },
  { name: 'city_san_fancisco', image: citySanFancisco },
  { name: 'city_santiago', image: citySantiago },
  { name: 'city_sao_paulo', image: citySaoPaulo },
  { name: 'city_shanghai', image: cityShanghai },
  { name: 'city_sydney', image: citySydney },
  { name: 'city_tokyo', image: cityTokyo },
  { name: 'city_toronto', image: cityToronto },
  { name: 'city_warsaw', image: cityWarsaw },
  { name: 'city_washington', image: cityWashington },
  { name: 'escalation', image: escalation },
  { name: 'event_a_holdover_from_the_war', image: eventAHoldoverFromTheWar },
  { name: 'event_airlift', image: eventAirlift },
  { name: 'event_counter_intelligence', image: eventCounterIntelligence },
  { name: 'event_cover_of_darkness', image: eventCoverOfDarkness },
  { name: 'event_forecast', image: eventForecast },
]

export const back = imageMap[0]
export const names = imageMap.map((i) => i.name)

export default imageMap
