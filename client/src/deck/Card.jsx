import React from 'react'
import { bool, oneOf } from 'prop-types'
import { find } from 'lodash'
import playerCards, { back as playerBack, names as playerNames } from './images/player/player'
import threatCards, { back as threatBack, names as threatNames } from './images/threat/threat'

const allCards = [...playerCards, ...threatCards]
const allCardNames = [...playerNames, ...threatNames]

export default function Card({ name, isPlayerCard, shouldFade }) {
  const card = find(allCards, { name })
  const src = card?.image || (isPlayerCard ? playerBack.image : threatBack.image)
  const alt = card?.name || (isPlayerCard ? playerBack.name : threatBack.name)

  return (
    <div className="rounded-3xl overflow-hidden shadow-lg h-80 w-60">
      <img className={`w-full h-full ${shouldFade ? 'opacity-40' : ''}`} src={src} alt={alt} />
    </div>
  )
}

Card.propTypes = {
  name: oneOf([...allCardNames, '']).isRequired,
  isPlayerCard: bool,
  shouldFade: bool,
}

Card.defaultProps = {
  isPlayerCard: true,
  shouldFade: false,
}
